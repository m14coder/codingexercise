import './App.css';
import Teams from './Components/Teams/Teams'

const localTeams = ['España','Argentina']

function App() {
  return (
    <div className="App">
      <Teams local={localTeams} away={localTeams}/>
    </div>
  );
}

export default App;
